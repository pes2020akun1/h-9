<?php
class Frog extends Animal{
    public $name,
           $legs,
           $cold_blooded;

    public function __construct($name, $legs, $cold_blooded) {
        $this->name = $name; 
        $this->legs = $legs; 
        $this->cold_blooded = $cold_blooded;
      }
      public function jump(){
        print "Hop hop";
      }
    }

    $kodok = new Frog("buduk", "4" , "no");
    echo "Name : $kodok->name"; // "buduk" 
    echo "<br>";
    echo "legs : $kodok->legs"; // 4
    echo "<br>";
    echo "cold blooded : $kodok->cold_blooded"; // "no"
    echo "<br>";
    echo "Jump : "; $kodok->jump(); // "hop hop"
    echo "<br>";
  echo "<br>";
?>