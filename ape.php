<?php

class Ape extends Animal{
    public $name,
           $legs,
           $cold_blooded;

    public function __construct($name, $legs, $cold_blooded) {
        $this->name = $name; 
        $this->legs = $legs; 
        $this->cold_blooded = $cold_blooded;
      }
      public function yell(){
        print "Auooo";
      }
    }

    $sungokong = new Ape("kera sakti", "2" , "no");
    echo "Name : $sungokong->name"; // "kera" 
    echo "<br>";
    echo "legs : $sungokong->legs"; // 2
    echo "<br>";
    echo "cold blooded : $sungokong->cold_blooded"; // "no"
    echo "<br>";
    echo "Yell : "; $sungokong->yell(); // "Auooo"
?>